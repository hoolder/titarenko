/**
 * Created by hoolder on 4/13/2016.
 */
public class SpecialCar2 extends Car {
    public SpecialCar2(String name, int maxSpeed, int acceleration, float mobility) {
        this.maxSpeed = maxSpeed;
        this.acceleration = acceleration;
        this.mobility = mobility;
        this.name = name;
    }

    public void specialSkill() {
        if (currentSpeed < maxSpeed / 2) {
            acceleration = acceleration * 2;
        }

    }
}
