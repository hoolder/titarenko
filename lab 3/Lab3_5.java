import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by hoolder on 4/11/2016.
 */
public class Lab3_5 {
    public static void main(String[] args) {
        float summa = 0.0F;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            while (true) {
                String temp = bufferedReader.readLine();
                if (temp.equals("сумма")){
                    System.out.println(summa);
                    break;
                }
                float temp1=Float.parseFloat(temp);
                summa+=temp1;
            }
        }
        catch (Exception e){
            System.out.println("you entered something wrong, see description of error: " + e.getMessage());
        }
    }
}
