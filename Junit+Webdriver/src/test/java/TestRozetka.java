import org.openqa.selenium.*;
import org.junit.*;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by hoolder on 5/26/2016.
 */
public class TestRozetka {
    private static WebDriver driver;
    private static String url;
    @BeforeClass
    public static void setUpRozetka(){
        driver = new FirefoxDriver();
        url="http://rozetka.com.ua/";
        driver.get(url);
        driver.manage().window().maximize();
    }

    @Test
    public void checkLogo(){
        WebElement logo = driver.findElement(By.xpath(".//img[@title='Интернет магазин Rozetka.ua™ - №1']"));
        Assert.assertTrue("Rozetka's logo is missing", logo.isDisplayed());

    }

    @Test
    public void appleLink(){
        WebElement appleLogo = driver.findElement(By.xpath(".//*[@href='http://rozetka.com.ua/apple/c4627486/'][@level='level1']"));
        Assert.assertTrue("Apple logo is missing", appleLogo.isDisplayed());
    }

    @Test
    public void mP3(){
    WebElement mp3 = driver.findElement(By.xpath(".//*[@id='m-main']/li/a[contains(text(),'MP3')]"));
    Assert.assertTrue(mp3.isDisplayed());
    }




    @Test
    public void city(){
        WebElement cities = driver.findElement(By.xpath(".//a[@class='novisited sprite-side header-city-select-link']/span"));
        cities.click();
        WebElement cityKiev=driver.findElement(By.xpath(".//*[@id='city-chooser']//*[contains(text(), 'Киев')]"));
        Assert.assertTrue(cityKiev.isDisplayed());
        WebElement cityKharkov=driver.findElement(By.xpath(".//*[@id='city-chooser']//*[contains(text(), 'Харьков')]"));
        Assert.assertTrue(cityKharkov.isDisplayed());
        WebElement cityOdessa=driver.findElement(By.xpath(".//*[@id='city-chooser']//*[contains(text(), 'Одесса')]"));
        Assert.assertTrue(cityOdessa.isDisplayed());

    }

    @Test
    public void basket(){
        WebElement bsk = driver.findElement(By.xpath(".//*[@id='body-header']/div[3]/div//*[contains(text(), 'Корзина')]"));
        bsk.click();
        WebElement emptyText = driver.findElement(By.xpath(".//h2[text()='Корзина пуста']"));
        Assert.assertTrue(emptyText.isDisplayed());
        WebElement close = driver.findElement(By.xpath(".//*[@id='cart-popup']/a/img"));
        close.click();
    }

    @AfterClass
    public static void setDown(){
        driver.quit();
    }


}
