package main.java.com.nixsolutions.Lab_7;
/**
 * Created by hoolder on 5/4/2016.
 */


//import com.sun.java.util.jar.pack.Package;

import java.io.*;
import java.lang.reflect.*;



public class Lab_7 {
    public static void main(String[] args) throws Exception{
        //Serialize
        Lab_7 lab7 = new Lab_7();
        lab7.ser();
        //UnSerialize
        Object unseril = lab7.unser();
        //reflection
        lab7.refl(unseril);

    }
    public boolean ser(){
       try{
           FileOutputStream fos = new FileOutputStream("temp.out");
           ObjectOutputStream oos = new ObjectOutputStream(fos);
           SerializeClass sc = new NewSerializeClass();
           oos.writeObject(sc);
           return true;
       }
       catch (IOException e){
           System.out.println(e.getMessage());
           return false;
       }
    }
    public Object unser(){
        try{
            FileInputStream fis = new FileInputStream("temp.out");
            ObjectInputStream oin = new ObjectInputStream(fis);
            SerializeClass sc1 = (SerializeClass) oin.readObject();
            //System.out.println(sc1.fild);
            return sc1;
        }
        catch (Exception e){
            System.out.println(e.getMessage());
            return false;
        }
    }
    public boolean refl(Object o){
        try{
            Class unknown  = o.getClass();
            Class unknownParent = unknown.getSuperclass();
            System.out.println(unknownParent.getName());
            Field[] fields = unknownParent.getDeclaredFields();
            Field field = fields[0];
            field.setAccessible(true);
            System.out.println("field: " + field.get(o));
            Method[] mets = unknownParent.getDeclaredMethods();
            Method method1 = mets[0];
            method1.setAccessible(true);
            method1.invoke(o);
            String result = (String)field.get(o);
            System.out.println("field: " + result);
            return true;
        }
        catch (Exception e){
            System.out.println(e.getMessage());
            return false;
        }
    }
}