import Rozetka.Basket;
import Rozetka.RozetkaMain;
import StackOverflow.*;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by hoolder on 5/30/2016.
 */
public class TeststackOverflow {
    private static WebDriver driver;
    private static String url;
    protected static  StackOverflow stackOverflow;
    protected static FirstQuestion firstQuestion;
    protected static SignUP signUP;


    @BeforeClass
    public static void setUpBla(){
        driver = new FirefoxDriver();
        url="http://stackoverflow.com/";
        driver.get(url);
        driver.manage().window().maximize();
        stackOverflow = new StackOverflow(driver);}


    @Test
    public void countFeaturedcomparison(){
        String temp = stackOverflow.countF.getText();
        int countt = Integer.parseInt(temp);
        //System.out.println(countt);
        boolean moreThan = false;
        if(countt>300){
            moreThan=true;
        }
        Assert.assertTrue(moreThan);
    }

    @Test
    public void zsocialIcons(){
        signUP=stackOverflow.openSignUp();
        Assert.assertTrue(signUP.facebook.isDisplayed() && signUP.google.isDisplayed());
        signUP.returnToMain();
    }

    @Test
    public void question()throws Exception{
        firstQuestion=stackOverflow.openfirstQuestion();
        String temp = firstQuestion.date.getAttribute("title");
        System.out.println(temp);
        //String temp="2016-05-26 13:55:00Z";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss'Z'");
        Date dateFromStack = format.parse(temp);
        System.out.println("date of last message: " + dateFromStack.getDate());
        Date currentDate = new Date();
        System.out.println("corrent date: " + currentDate.getDate());
        Assert.assertTrue(dateFromStack.getDate()==currentDate.getDate());
        //
        firstQuestion.returnToMain();

    }

    @Test
    public void money()
    {
        boolean mon=false;
        try
        {
            TimeUnit.SECONDS.sleep(5);
            String temp = stackOverflow.moneyOffer.getText();
            //String temp = "£40,000 - £60,000";
            String[] splited = temp.split(" - ");
            String lastValueMoney;
            if(splited[1].contains("£")){
            lastValueMoney = splited[1].replaceAll("£", "");}
            else if(splited[1].contains("$")){
                lastValueMoney=splited[1].replaceAll("$", "");
            }
            else{
                lastValueMoney=splited[1].replaceAll("€", "");
            }
            System.out.println("Salary is: " + lastValueMoney);
            lastValueMoney=lastValueMoney.replaceAll(",", "");
            lastValueMoney=lastValueMoney.replaceAll(" ", "");
            //System.out.println(lastValueMoney);
            //int money = Integer.parseInt(lastValueMoney);

            try{
                int money = Integer.parseInt(lastValueMoney);
                if(money>100000)
                {
                    mon=true;
                }
            }
            catch (Exception e)
            {
                mon=false;
            }
            Assert.assertTrue("there is no info about amount of salary OR salary is less than 100000", mon);
        }
        catch (Exception e){
            Assert.assertTrue("there is no info about amount of salary OR salary is less than 100000", mon);
        }


    }
    @AfterClass
    public static void setDown(){
        driver.quit();
    }
}
