package Rozetka.Run;

import Rozetka.Basket;
import Rozetka.RozetkaMain;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by hoolder on 6/2/2016.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/test/java/Rozetka/features"},
        glue = {"Rozetka/steps"},
        tags = {"@fullTestRozetka"}
)
public class Runner {
    public static WebDriver driver;
    public static String url;
   public static RozetkaMain rozetkaMain;
    public static Basket basket;


    @BeforeClass
    public static void setUpBla(){
        driver = new FirefoxDriver();
        url="http://rozetka.com.ua/";
        driver.get(url);
        driver.manage().window().maximize();
        rozetkaMain = new RozetkaMain(driver);
    }



    @AfterClass
    public static void setDown(){
        driver.quit();
    }

}
