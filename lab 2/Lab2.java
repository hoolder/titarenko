/**
 * Created by hoolder on 4/6/2016.
 */
public class Lab2 {
    public static void main(String[] args) {
        int x=10;
        int y=5;
        int z=0;
        String temp;
       // First part:
        System.out.println("First part:");
        //Arithmetic
        System.out.println("x=" + x + " y=" + y);
        z=x+y;
        System.out.println("x+y=" + z);
        z=x-y;
        System.out.println("x-y=" + z);
        z=x*y;
        System.out.println("x*y=" + z);
        z=x/y;
        System.out.println("x/y=" + z);
        //comparison
        boolean bool=false;
        bool=x>y;
        System.out.println("x>y=" + bool);
        bool=x<y;
        System.out.println("x<y=" + bool);
        //logic
        bool=x>y && x<100;
        System.out.println("x>y and x<100?  - " + bool);
        bool=x<y || x<100;
        System.out.println("x>y OR x<100?  - " + bool);
        //increment and so on
        x++;
        System.out.println("x++=" + x);
        x=y;
        System.out.println("if x=y then x will be equal to:  " + x);
        // Second part
        System.out.println();
        System.out.println("Second part:");
        temp = Integer.toString(x);
        System.out.println("int to String: " + temp);
        x=Integer.parseInt(temp);
        System.out.println("String to int: " + x);
        long longX = 100L;
        temp = Long.toString(longX);
        System.out.println("long to Sring: " + temp);
        longX=Long.parseLong(temp);
        System.out.println("String to long: " + longX);
        float floatX= 1.1F;
        temp=Float.toString(floatX);
        System.out.println("float to String: " + temp);
        floatX=Float.parseFloat(temp);
        System.out.println("String to float: " + floatX);
        double doubleX=1.1D;
        temp=Double.toString(doubleX);
        System.out.println("double to String: " + temp);
        doubleX=Double.parseDouble(temp);
        System.out.println("String to double: " + doubleX);
        char charX='a';
        temp=Character.toString(charX);
        System.out.println("char to String: " + temp);
        charX=temp.charAt(0);
        System.out.println("String to char: " + charX);
        temp=Boolean.toString(bool);
        System.out.println("bool to String: " + temp);
        bool=Boolean.parseBoolean(temp);
        System.out.println("String to bool: " + bool);
        //Third part
        System.out.println();
        System.out.println("Third part:");
        x=(int)floatX;
        System.out.println("float 1.1 to int = " + x);
        floatX=(float)x;
        System.out.println("int 1 to float = " + floatX);
    }
}
