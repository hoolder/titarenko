import junit.framework.Assert;
import org.junit.Test;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Артем on 16.06.2016.
 */
public class test_API {

    @Test
    public void testURLS() throws IOException {
        ArrayList<String> responses;
        //read urls and expected from file
        API api = new API();
        int count=0;
       List<URL> listurls=api.readURL();
        for(URL url:listurls){
            boolean status=false;
            // get response for url
            responses=api.getResponse(url);
            // expected for response
            String temp = api.listExpected.get(count);
            count++;
            for(String s: responses){
              if(s.contains(temp)){
                  status=true;
              }
            }
            Assert.assertTrue(status);
        }

    }

}
