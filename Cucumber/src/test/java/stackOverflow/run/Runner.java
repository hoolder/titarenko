package stackOverflow.run;

/**
 * Created by hoolder on 6/2/2016.
 */
import StackOverflow.*;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by hoolder on 6/2/2016.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/test/java/StackOverflow/features"},
        glue = {"stackOverflow/steps"},
        tags = {"@fullTestStackOverflow"}
)
public class Runner {
    public static WebDriver driver;
    public static String url;
    public static  StackOverflow stackOverflow;
    public static FirstQuestion firstQuestion;
    public static SignUP signUP;


    @BeforeClass
    public static void setUpBla(){
        driver = new FirefoxDriver();
        url="http://stackoverflow.com/";
        driver.get(url);
        driver.manage().window().maximize();
        stackOverflow = new StackOverflow(driver);
    }



    @AfterClass
    public static void setDown(){
        driver.quit();
    }

}
