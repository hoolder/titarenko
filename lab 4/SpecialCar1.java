/**
 * Created by hoolder on 4/13/2016.
 */
public class SpecialCar1 extends Car {
    public SpecialCar1(String name, int maxSpeed, int acceleration, float mobility) {
        this.maxSpeed = maxSpeed;
        this.acceleration = acceleration;
        this.mobility = mobility;
        this.name = name;
    }

    public void specialSkill() {
        if (currentSpeed > (maxSpeed / 2)) {
            // use 0.5% as bonus
            mobility= (float) (mobility + 0.005*(currentSpeed-(maxSpeed/2)));
            //System.out.println(mobility);

        }

    }
}
