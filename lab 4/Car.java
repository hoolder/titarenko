/**
 * Created by hoolder on 4/13/2016.
 */
public abstract class Car {
    String name;
    int maxSpeed;
    int acceleration;
    float mobility;
    int currentSpeed = 0;
    abstract void  specialSkill();
    int time=0;
    public int trace() {
        int time = 0;
        int put=2000;
        for (int i = 0; i < 20; i++) {
            time=time+(int)(Math.sqrt(currentSpeed*currentSpeed + 2*acceleration*put)-currentSpeed)/acceleration;
            currentSpeed=(currentSpeed+acceleration*time);
            if(currentSpeed>maxSpeed){
                currentSpeed=maxSpeed;
            }
            specialSkill();
            //System.out.println(car.currentSpeed);
            //System.out.println(car.maxSpeed);
            currentSpeed=(int)(currentSpeed*mobility);

        }
        return time;
    }

}
