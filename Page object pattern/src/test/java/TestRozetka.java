import Rozetka.Basket;
import Rozetka.RozetkaMain;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by hoolder on 5/30/2016.
 */
public class TestRozetka {
    private static WebDriver driver;
    private static String url;
    protected static RozetkaMain rozetkaMain;
    protected static Basket basket;

    @BeforeClass
    public static void setUpBla(){
        driver = new FirefoxDriver();
        url="http://rozetka.com.ua/";
        driver.get(url);
        driver.manage().window().maximize();
        rozetkaMain = new RozetkaMain(driver);

    }

    @Test
    public void checkLogo(){
        Assert.assertTrue("no log", rozetkaMain.logo.isDisplayed());
    }

    @Test
    public void appleLink(){
        Assert.assertTrue("no apple logo", rozetkaMain.appleLogo.isDisplayed());
    }

    @Test
    public void mp3(){
        Assert.assertTrue("no Mp3", rozetkaMain.mp3.isDisplayed());
    }

    @Test
    public void cities(){
        rozetkaMain.cities.click();
        Assert.assertTrue(rozetkaMain.cityKharkov.isDisplayed() && rozetkaMain.cityKiev.isDisplayed() && rozetkaMain.cityOdessa.isDisplayed());
    }

    public void basketTest(){
        basket=rozetkaMain.openBasket();
        Assert.assertTrue(basket.emptyText.isDisplayed());
        basket.close();
    }

    @AfterClass
    public static void setDown(){
        driver.quit();
    }


}
