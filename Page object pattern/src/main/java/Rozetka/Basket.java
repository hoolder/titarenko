package Rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by hoolder on 5/30/2016.
 */
public class Basket {
    private WebDriver driver;

    public Basket(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//h2[text()='Корзина пуста']")
    public WebElement emptyText;

    @FindBy(xpath = ".//*[@id='cart-popup']/a/img")
    public WebElement close;

    public void close(){
        close.click();
    }
}
