import java.io.File;
import java.util.ArrayList;

/**
 * Created by hoolder on 4/19/2016.
 */
public class ProcessFilesFromFolder {
    ArrayList<File> files = new ArrayList<>();
    public ArrayList<File> processFilesFromFolder(File folder)
    {
        File[] folderEntries = folder.listFiles();
        for (File entry : folderEntries)
        {
            if (entry.isDirectory())
            {
                processFilesFromFolder(entry);
                continue;
            }
            files.add(entry);
            //System.out.println(entry.getAbsoluteFile());
        }
        return files;
    }
}
