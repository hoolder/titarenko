package main.java.com.nixsolutions.Lab_7;

import org.junit.*;


import static org.junit.Assert.*;

/**
 * Created by hoolder on 5/5/2016.
 */
public class Lab_7Test {
    @Test

    public void testSer()throws Exception{
        Lab_7 lab7 = new Lab_7();
        assertTrue(lab7.ser());
    }

    @Test
    public void testUnser() throws Exception{
        Lab_7 lab7 = new Lab_7();
        SerializeClass sc = new NewSerializeClass();
        assertEquals(sc.getClass(), lab7.unser().getClass());
    }

    @Test
    public void testRefl(){
        Lab_7 lab7 = new Lab_7();
        assertTrue(lab7.refl(lab7.unser()));
    }

}
