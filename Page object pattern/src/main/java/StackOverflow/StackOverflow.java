package StackOverflow;

/**
 * Created by hoolder on 5/30/2016.
 */
import org.openqa.selenium.*;
import org.junit.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
public class StackOverflow {
    private WebDriver driver;

    public StackOverflow(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//*[@id='tabs']/a[2]/span")
    public WebElement countF;

    @FindBy(xpath = ".//a[text()='sign up']")
    public WebElement signup;

    public SignUP openSignUp(){
        signup.click();
        return new SignUP(driver);
    }

    @FindBy(xpath = ".//div[@id='question-mini-list']/div[1]/div[2]/h3/a")
    public WebElement firstQ;

    public FirstQuestion openfirstQuestion(){
        firstQ.click();
        return new FirstQuestion(driver);
    }

    @FindBy(xpath = ".//*[@id='hireme']//*[@class='salary']")
    public WebElement moneyOffer;








}
