package stackOverflow.steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import stackOverflow.run.Runner;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by hoolder on 6/2/2016.
 */
public class MyStepdefs {
    @Given("^I on StackOverflow main page$")
    public void iOnStackOverflowMainPage()  {
        // Write code here that turns the phrase above into concrete actions
        Runner.driver.get(Runner.url);
    }

    @When("^I look at$")
    public void iLookAt()  {
        // Write code here that turns the phrase above into concrete actions

    }

    @Then("^count is bigger then three hundreds$")
    public void countIsBiggerThenThreeHundreds()  {
        // Write code here that turns the phrase above into concrete actions
        String temp = Runner.stackOverflow.countF.getText();
        int countt = Integer.parseInt(temp);
        //System.out.println(countt);
        boolean moreThan = false;
        if(countt>300){
            moreThan=true;
        }
        Assert.assertTrue(moreThan);
    }

    @When("^I click on sign up$")
    public void iClickOnSignUp()  {
        // Write code here that turns the phrase above into concrete actions
        Runner.signUP = Runner.stackOverflow.openSignUp();
    }

    @Then("^I see Social icons$")
    public void iSeeSocialIcons()  {
        // Write code here that turns the phrase above into concrete actions
        Assert.assertTrue(Runner.signUP.facebook.isDisplayed() && Runner.signUP.google.isDisplayed());
    }

    @When("^I open latest question$")
    public void iOpenLatestQuestion()  {
        // Write code here that turns the phrase above into concrete actions
        Runner.firstQuestion = Runner.stackOverflow.openfirstQuestion();
    }

    @Then("^I see that the date of question is the same as today$")
    public void iSeeThatTheDateOfQuestionIsTheSameAsToday() throws Exception {
        // Write code here that turns the phrase above into concrete actions
        String temp = Runner.firstQuestion.date.getAttribute("title");
        System.out.println(temp);
        //String temp="2016-05-26 13:55:00Z";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss'Z'");
        Date dateFromStack = format.parse(temp);
        System.out.println("date of last message: " + dateFromStack.getDate());
        Date currentDate = new Date();
        System.out.println("current date: " + currentDate.getDate());
        Assert.assertTrue(dateFromStack.getDate()==currentDate.getDate());

    }

    @Then("^I see that money offer is higher than specific count$")
    public void iSeeThatMoneyOfferIsHigherThanSpecificCount()  {
        // Write code here that turns the phrase above into concrete actions
        boolean mon=false;
        try
        {
            TimeUnit.SECONDS.sleep(5);
            String temp = Runner.stackOverflow.moneyOffer.getText();
            //String temp = "£40,000 - £60,000";
            String[] splited = temp.split(" - ");
            String lastValueMoney;
            if(splited[1].contains("£")){
                lastValueMoney = splited[1].replaceAll("£", "");}
            else if(splited[1].contains("$")){
                lastValueMoney=splited[1].replaceAll("$", "");
            }
            else{
                lastValueMoney=splited[1].replaceAll("€", "");
            }
            System.out.println("Salary is: " + lastValueMoney);
            lastValueMoney=lastValueMoney.replaceAll(",", "");
            lastValueMoney=lastValueMoney.replaceAll(" ", "");
            //System.out.println(lastValueMoney);
            //int money = Integer.parseInt(lastValueMoney);

            try{
                int money = Integer.parseInt(lastValueMoney);
                if(money>100000)
                {
                    mon=true;
                }
            }
            catch (Exception e)
            {
                mon=false;
            }
            Assert.assertTrue("there is no info about amount of salary OR salary is less than 100000", mon);
        }
        catch (Exception e){
            Assert.assertTrue("there is no info about amount of salary OR salary is less than 100000", mon);
        }
    }
}
