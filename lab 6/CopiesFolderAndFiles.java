import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

/**
 * Created by Артем on 28.04.2016.
 */
public class CopiesFolderAndFiles extends Thread
{
    private static int newName =1;
    public String path;
    public String searchTerm;

    public CopiesFolderAndFiles(String path, String searchTerm) {
        this.path = path;
        this.searchTerm=searchTerm;
    }

    public void run()
        {
            //System.out.println("thread starts!");


            try {
                //Thread.sleep(10000);


                //File folder = new File("c:\\1\\");
                File folder = new File(path);
                ArrayList<File> files = null;
                ProcessFilesFromFolder p = new ProcessFilesFromFolder();
                files = p.processFilesFromFolder(folder);

                char[] originalPath = path.toCharArray();
                for (File f : files) {
                    Path Origina = Paths.get(f.getPath());
                    if (f.getName().contains(searchTerm)) {
                        char[] filePath = f.getPath().toCharArray();
                        int temp = 0;
                        for (int i = 0; i < originalPath.length; i++) {
                            if (filePath[i] == originalPath[i]) {
                                temp = i;
                            }
                        }
                        //System.out.println(temp);
                        String cutted = f.getPath().substring(temp + 1);
                        //System.out.println(cutted);
                        if (cutted.contains("\\")) {
                            char[] doubleCuttedPath = cutted.toCharArray();
                            String folderForCopy = "";
                            String FullPathForCopy;
                            int endOfFolder = 0;
                            for (int i = 0; i < doubleCuttedPath.length; i++) {
                                folderForCopy += doubleCuttedPath[i];
                                endOfFolder = i;
                                if (doubleCuttedPath[i] == '\\') {
                                    FullPathForCopy = path + "new folder " + newName + " " + folderForCopy + cutted.substring(endOfFolder + 1);
                                    newName++;
                                    //System.out.println(FullPathForCopy);
                                    Path FullWithFolder = Paths.get(FullPathForCopy);

                                    File neFile1 = new File(FullPathForCopy);
                                    neFile1.mkdirs();
                                    Files.copy(f.toPath(), neFile1.toPath(), StandardCopyOption.REPLACE_EXISTING);
                                    System.out.println(FullWithFolder + " is created");
                                    break;
                                }
                            }
                        } else {
                            cutted = "new file" + newName + " " + cutted;
                            newName++;
                            File neFile = new File(path + cutted);
                            Files.copy(Origina, neFile.toPath());
                            System.out.println(cutted);
                        }


                    }
                }

            }
            catch (Exception e){
                System.out.println(e.getMessage());
            }

        }
}
