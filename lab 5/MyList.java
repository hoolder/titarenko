


import java.util.*;


/**
 * Created by hoolder on 4/13/2016.
 */
public class MyList implements List  {
    private Object[] myList;
    //private int newSize=0;

    public MyList(Object[] myList) {
        this.myList = myList;
    }


    @Override
    public synchronized int size()
    {
        return myList.length;
    }

    @Override
    public boolean isEmpty() {
       if(myList.length == 0){
           return true;
       }
        else return false;
    }

    @Override
    public boolean contains(Object o) {
        int temp=0;
        for(Object d: myList){
            if(d.equals(o)){
                temp++;
            }
        }
        if(temp>0){
            return true;
        }
        else
            return false;
    }
    //Iterator
    @Override

    public Iterator iterator() {
        return new Itr();
    }

    private class Itr implements Iterator{
    int cursor=0;
        @Override
        public boolean hasNext() {
            if(cursor<myList.length)
                return true;
            else
                return false;
        }

        @Override
        public Object next() {
            cursor++;
            return myList[cursor];
        }
    }


        @Override
    public Object[] toArray() {
        return myList;
    }


    @Override
    public boolean add(Object d) {
        //newSize++;
       myList= Arrays.copyOf(myList, myList.length+1);
       myList[myList.length-1]=d;
       return true;
    }
    ///
    @Override
    public boolean remove(Object o) {
        boolean count=false;
        for (int i = 0; i < myList.length; i++) {
            if(myList[i].equals(o)){
          MyList.this.remove(i);
                break;
            }
        }
        return count;
    }

    @Override
    public boolean addAll(Collection c) {
        boolean count=true;
        Object[] a = c.toArray();
        Object[] copy = new Object[myList.length + a.length];
        System.arraycopy(myList, 0, copy, 0, myList.length);
        System.arraycopy(a, 0, copy, myList.length, a.length);
        myList=copy;
        return count;
    }

    @Override
    public boolean addAll(int index, Collection c) {
        boolean count=true;
        Object[] a = c.toArray();
        Object[] copy = new Object[myList.length + a.length];
        System.arraycopy(myList, 0, copy, 0, index);
        System.arraycopy(a, 0, copy, index, a.length);
        System.arraycopy(myList, index+1, copy, index+a.length, myList.length);
        myList=copy;
        return count;
    }





    @Override
    public void clear() {
    Object[] o = new Object[0];
    myList=o;
    }

    @Override
    public Object get(int index) {
    return myList[index];
    }

    @Override
    public Object remove(int index) {
        if (index >= 0 && index < myList.length)
        {
            Object[] copy = new Object[this.myList.length-1];
            System.arraycopy(myList, 0, copy, 0, index);
            System.arraycopy(myList, index+1, copy, index, myList.length-index-1);
            myList=copy;
        }
        return null;
    }

    @Override
    public Object set(int index, Object element) {
        myList[index]=element;
        return null;
    }

    @Override
    public void add(int index, Object element) {
        if (index >= 0 && index < myList.length)
        {
            Object[] copy = new Object[this.myList.length+1];
            System.arraycopy(myList, 0, copy, 0, index);
            copy[index]=element;
            System.arraycopy(myList, index+1, copy, index, myList.length-index);
            myList=copy;
        }

    }



    @Override
    public int indexOf(Object o) {
        int index=-1;
        for (int i = 0; i < myList.length; i++) {
            if(myList[i].equals(o)){
                index=i;
                break;
            }
        }
        return index;
    }

    @Override
    public int lastIndexOf(Object o) {
        int index=-1;
        for (int i = 0; i < myList.length; i++) {
            if(myList[i].equals(o)){
                index=i;
            }
        }
        return index;
    }

    @Override
    public ListIterator listIterator() {
        return new ListItr(0);
    }


    @Override
    public ListIterator listIterator(int index) {
        return new ListItr(index);
    }

    private class ListItr extends Itr implements ListIterator {
        ListItr(int index) {
            super();
            cursor = index;
        }


        @Override
        public boolean hasPrevious() {
            return cursor != 0;
        }

        @Override
        public Object previous() {
            int i = cursor - 1;
            cursor = i;
            return myList[cursor];

        }

        @Override
        public int nextIndex() {
            return cursor;
        }

        @Override
        public int previousIndex() {
            return cursor - 1;
        }

        @Override
        public void remove() {
        MyList.this.remove(cursor);
        }

        @Override
        public void set(Object o) {
        MyList.this.set(cursor, o);
        }

        @Override
        public void add(Object o) {
            MyList.this.add(o);

        }
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        Object[] copy = new Object[toIndex-fromIndex];
        System.arraycopy(myList, fromIndex, copy, 0, toIndex);
        return new MyList(copy);
    }

    @Override
    public boolean retainAll(Collection c) {
        for (int i = 0; i < myList.length; i++) {
            int tmp=0;
            for (Object o: c){
                if(o.equals(myList[i])){
                    tmp++;
                }
            }
            if(tmp==0){
                MyList.this.remove(i);
            }
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection c) {
        for(Object o: c){
           for (int i = 0; i < myList.length; i++) {
               if(o.equals(myList[i])){
                   MyList.this.remove(i);
                   i--;
               }
           }
       }
       return true;
    }

    @Override
    public boolean containsAll(Collection c) {
        boolean temp=true;

        for(Object iter: c){
            int tmp=0;
            for (int i = 0; i < myList.length; i++) {

                if(iter.equals(myList[i])){
                    tmp++;
                }
            }
            if (tmp==0){
                temp=false;
                break;
            }
        }
        return temp;
    }

    @Override
    public Object[] toArray(Object[] a) {
        return myList;
    }
}
