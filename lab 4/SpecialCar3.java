/**
 * Created by hoolder on 4/13/2016.
 */
public class SpecialCar3 extends Car {
    public SpecialCar3(String name, int maxSpeed, int acceleration, float mobility)
    {
        this.maxSpeed = maxSpeed;
        this.acceleration = acceleration;
        this.mobility = mobility;
        this.name = name;
    }

    public void specialSkill() {
        if (currentSpeed >= maxSpeed ) {
            maxSpeed=(int)(maxSpeed+maxSpeed*0.1);
        }

    }
}
