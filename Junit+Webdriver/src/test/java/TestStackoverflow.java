import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by hoolder on 5/26/2016.
 */
public class TestStackoverflow {
    private static WebDriver driver;
    private static String url;
    @BeforeClass
    public static void setUpstackBlaBla(){
        driver = new FirefoxDriver();
        url="http://stackoverflow.com/";
        driver.get(url);
        driver.manage().window().maximize();
    }
    @Test
    public void countFeaturedcomparison(){
        WebElement countF = driver.findElement(By.xpath(".//*[@id='tabs']/a[2]/span"));
        String temp = countF.getText();
        int countt = Integer.parseInt(temp);
        //System.out.println(countt);
        boolean moreThan = false;
        if(countt>300){
            moreThan=true;
        }
        Assert.assertTrue(moreThan);
    }



    @Test
    public void zsocialIcons(){
        WebElement signup = driver.findElement(By.xpath(".//a[text()='sign up']"));
        signup.click();
        WebElement google = driver.findElement(By.xpath("//*[text()='Google']"));
        Assert.assertTrue(google.isDisplayed());
        WebElement facebook = driver.findElement(By.xpath("//*[text()='Facebook']"));
        Assert.assertTrue(facebook.isDisplayed());

    }

    @Test
    public void question()throws Exception{
        WebElement firstQ = driver.findElement(By.xpath(".//div[@id='question-mini-list']/div[1]/div[2]/h3/a"));
        firstQ.click();
        WebElement date = driver.findElement(By.xpath(".//*[@id='qinfo']/tbody/tr[1]/td[2]/p"));
        String temp = date.getAttribute("title");
        System.out.println(temp);
        //String temp="2016-05-26 13:55:00Z";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss'Z'");
        Date dateFromStack = format.parse(temp);
        System.out.println(dateFromStack.getDate());
        Date currentDate = new Date();
        System.out.println(currentDate.getDate());
        Assert.assertTrue(dateFromStack.getDate()==currentDate.getDate());
    }

    @Test
    public void money()
    {
        boolean mon=false;
        try
        {
        WebElement moneyOffer = driver.findElement(By.xpath(".//*[@id='hireme']//*[@class=\"salary\"]"));
            String temp = moneyOffer.getText();
            //String temp = "£40,000 - £60,000";
            String[] splited = temp.split(" - ");
            String lastValueMoney = splited[1].replaceAll("£", "");
            lastValueMoney=lastValueMoney.replaceAll(",", "");
            lastValueMoney=lastValueMoney.replaceAll(" ", "");
            //System.out.println(lastValueMoney);
            //int money = Integer.parseInt(lastValueMoney);

            try{
                int money = Integer.parseInt(lastValueMoney);
                if(money>100000)
                {
                    mon=true;
                }
            }
            catch (Exception e)
            {
                mon=false;
            }
            Assert.assertTrue("there is no info about amount of salary OR salary is less than 100000", mon);
        }
        catch (Exception e){
            Assert.assertTrue("there is no info about amount of salary OR salary is less than 100000", mon);
        }


    }

    @AfterClass
    public static void setDown(){
        driver.quit();
    }
}
