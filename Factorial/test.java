import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;

/**
 * Created by hoolder on 6/30/2016.
 */
public class test {
    public static void main(String[] args)  {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while(true){
        try{
        int input = Integer.parseInt(reader.readLine());
            System.out.println(factorial(input));
        break;
        }
        catch (Exception e){
            System.out.println("you entered not number, try again");
        }
        }
    }

    public static String factorial(int n) {
        String temp;
        if(n <= 0) {
            temp = "0";
            return temp;
        } else {
            BigInteger x = BigInteger.valueOf(1L);

            for(int i = 1; i < n + 1; ++i) {
                x = x.multiply(BigInteger.valueOf((long)i));
            }

            temp = x.toString();
            return temp;
        }
    }
}
