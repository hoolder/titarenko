package Rozetka;

/**
 * Created by hoolder on 5/30/2016.
 */
import org.openqa.selenium.*;
import org.junit.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RozetkaMain {
    private WebDriver driver;

    public RozetkaMain(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//img[@title='Интернет магазин Rozetka.ua™ - №1']")
    public WebElement logo;

    @FindBy(xpath = ".//*[@href='http://rozetka.com.ua/apple/c4627486/'][@level='level1']")
    public WebElement appleLogo;

    @FindBy(xpath = ".//*[@id='m-main']/li/a[contains(text(),'MP3')]")
    public WebElement mp3;

    @FindBy(xpath = ".//a[@class='novisited sprite-side header-city-select-link']/span")
    public WebElement cities;

    @FindBy(xpath = ".//*[@id='city-chooser']//*[contains(text(), 'Киев')]")
    public WebElement cityKiev;
    @FindBy(xpath = ".//*[@id='city-chooser']//*[contains(text(), 'Харьков')]")
    public WebElement cityKharkov;
    @FindBy(xpath = ".//*[@id='city-chooser']//*[contains(text(), 'Одесса')]")
    public WebElement cityOdessa;

    @FindBy(xpath = ".//*[@id='body-header']//*[contains(text(), 'Корзина')]")
    public WebElement bsk;


    public Basket openBasket(){
        bsk.click();
        return new Basket(driver);
    }






}
