

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by hoolder on 4/6/2016.
 */
public class Lab2_2 {
    public static void main(String[] args) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("enter first number");
        float result=0;

        try{
            float firstNumber = Float.parseFloat(bufferedReader.readLine());
            System.out.println("enter operator");
            char operator = bufferedReader.readLine().charAt(0);
            System.out.println("enter second number");
            float secondNumber = Float.parseFloat(bufferedReader.readLine());

            switch (operator){
                case '+':{
                    result=firstNumber+secondNumber;
                    if(result%1==0)
                        System.out.println("you result is " + (int)result);
                    else
                        System.out.println("you result is " + result);
                    break;
                }
                case '-': {
                    result=firstNumber-secondNumber;
                    if(result%1==0)
                        System.out.println("you result is " + (int)result);
                    else
                        System.out.println("you result is " + result);
                    break;
                }
                case '*':{
                    result=firstNumber*secondNumber;
                    if(result%1==0)
                        System.out.println("you result is " + (int)result);
                    else
                        System.out.println("you result is " + result);
                    break;
                }
                case '/':{
                    result=firstNumber/secondNumber;
                    if(result%1==0)
                    System.out.println("you result is " + (int)result);
                    else
                        System.out.println("you result is " + result);
                    break;
                }
                default:
                    System.out.println("system cannot recognize your arithmetic operation");
            }


            }
        catch (Exception e){
            System.out.println("you entered NOT number: " + e.getMessage());
        }



}
}
