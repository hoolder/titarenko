package StackOverflow;

/**
 * Created by hoolder on 5/30/2016.
 */
import org.openqa.selenium.*;
import org.junit.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
public class SignUP {
    private WebDriver driver;

    public SignUP(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
    @FindBy(xpath = "//*[text()='Google']")
    public WebElement google;

    @FindBy(xpath = "//*[text()='Facebook']")
    public WebElement facebook;

    @FindBy(xpath = ".//*[@id='hlogo']/a")
    public WebElement logo;

    public StackOverflow returnToMain(){
        logo.click();
        return new StackOverflow(driver);
    }

}
