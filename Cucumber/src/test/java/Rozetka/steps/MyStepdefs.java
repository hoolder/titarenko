package Rozetka.steps;

import Rozetka.RozetkaMain;
import Rozetka.Basket;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import Rozetka.Run.*;
import static org.junit.Assert.*;


/**
 * Created by hoolder on 6/2/2016.
 */
public class MyStepdefs {
    @Given("^I on Rozetka main page$")
    public void iOnRozetkaMainPage()  {
        Runner.driver.get(Runner.url);
        // Write code here that turns the phrase above into concrete actions
        //assertTrue(Runner.driver.getCurrentUrl().equals("Rozetka.com.ua"));
    }



    @Then("^I see Rozetka Logo$")
    public void iSeeRozetkaLogo()  {
        // Write code here that turns the phrase above into concrete actions
        assertTrue(Runner.rozetkaMain.logo.isDisplayed());
    }



    @Then("^I see apple logo$")
    public void iSeeAppleLogo()  {
        // Write code here that turns the phrase above into concrete actions
        assertTrue(Runner.rozetkaMain.appleLogo.isDisplayed());
    }

    @When("^I look$")
    public void iLook() {
        // Write code here that turns the phrase above into concrete actions

    }


    @Then("^I see mpThree$")
    public void iSeeMpThree()  {
       assertTrue(Runner.rozetkaMain.mp3.isDisplayed());
    }

    @When("^I click cities$")
    public void iClickCities()  {
        // Write code here that turns the phrase above into concrete actions
       Runner.rozetkaMain.cities.click();
    }

    @Then("^I see specific cities$")
    public void iSeeSpecificCities()  {
        // Write code here that turns the phrase above into concrete actions
        assertTrue(Runner.rozetkaMain.cityKiev.isDisplayed() && Runner.rozetkaMain.cityOdessa.isDisplayed() && Runner.rozetkaMain.cityKharkov.isDisplayed());
    }

    @When("^I click on Basket$")
    public void iClickOnBasket()  {
        // Write code here that turns the phrase above into concrete actions
        Runner.basket=Runner.rozetkaMain.openBasket();
    }

    @Then("^I see empty Basket$")
    public void iSeeEmptyBasket()  {
        // Write code here that turns the phrase above into concrete actions
        assertTrue(Runner.basket.emptyText.isDisplayed());
    }
}
