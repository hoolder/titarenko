package main.java.com.nixsolutions.Lab_7;


import org.junit.Test;

import java.lang.reflect.*;


import static org.junit.Assert.*;

/**
 * Created by hoolder on 5/4/2016.
 */
public class SerializeClassTest {

    @Test
    public void testChangeField() throws Exception {
        SerializeClass serializeClass = new SerializeClass();
        Class TestSC = serializeClass.getClass();
        System.out.println(TestSC.getSimpleName());
        Field[] fields = TestSC.getDeclaredFields();
        Field field = fields[0];
        field.setAccessible(true);
        System.out.println(field.getName());
        String fld = (String) field.get(serializeClass);
        //System.out.println(field.getModifiers());
        Method[] mets = TestSC.getDeclaredMethods();
        Method method1 = mets[0];
        method1.setAccessible(true);
        String result = (String)field.get(serializeClass);
        System.out.println(result);
        assertEquals("start", result);
        method1.invoke(serializeClass);
        result = (String)field.get(serializeClass);
        System.out.println(result);
        assertEquals("finish", result);
    }
}