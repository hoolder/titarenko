import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Артем on 16.06.2016.
 */


public class API {


    static HttpURLConnection connection = null;
    static File file =new File("src/urls.txt");

    static ArrayList<String> listExpected = new ArrayList<String>();

    public static void main(String[] args) throws Exception
    {


    }

    public  List<URL> readURL() throws IOException {
        BufferedReader bf = new BufferedReader(new FileReader(file));
        String read = null;
        ArrayList<URL> listURLs = new ArrayList<URL>();
        while ((read = bf.readLine()) != null) {
            String[] splited = read.split("   ");
            listURLs.add(new URL(splited[0]));
            listExpected.add(splited[1]);
        }
        return listURLs;
    }

    public  ArrayList<String> getResponse(URL url) throws IOException {
        ArrayList<String> response = new ArrayList<String>();
        connection=(HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        String temp;
        BufferedReader responseReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        while ((temp = responseReader.readLine()) != null) {
            System.out.println(temp);
            response.add(temp);
        }
        return response;
    }

    public  boolean validation(List<String> list, String expected){
        boolean result = false;
        for(String s: list){
            if(s.contains(expected)){
                result=true;
            }
        }
        return result;
    }
}
